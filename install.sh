#!/bin/bash

set -e

if [[ $EUID -ne 0 ]]; then
    echo "Run as root"
    exit 1
fi

ln -sf "$(pwd -P)"/nginx/linx.conf /etc/nginx/sites-enabled/linx.conf

cat > /etc/systemd/system/linx-server.service <<EOF
[Unit]
Description=Linx Server
After=nginx.service
Requires=nginx.service

[Service]
User=nginx
Group=nginx
ExecStart=/opt/linx-server/linx-server -config /opt/linx-server/config.ini

[Install]
WantedBy=multi-user.target
EOF

cat /etc/systemd/system/linx-server.service

mkdir -p /opt/linx-server/meta
mkdir -p /var/www/linx

cat > /opt/linx-server/config.ini <<EOF
bind = 127.0.0.1:5469
maxsize = 4294967296
allowhotlink = true
sitename = Dre Linx
siteurl = https://linx.dresrv.com/
realip = true
fastcgi = true
filespath = /var/www/linx/
metapath = /opt/linx-server/meta/
nologs = true
authfile = /opt/linx-server/auth
EOF

read -p "Password for uploads: " upload_pass
hashed_pass=$(bin/linx-genkey <<< "$upload_pass" | sed s/"Enter key to hash: "//g)
echo "$hashed_pass" > /opt/linx-server/auth

cat /opt/linx-server/config.ini

./update-linx.sh

echo "Setting permissions"

chown root:root -R /opt/linx-server
chmod 755 -R /opt/linx-server
chmod 644 /opt/linx-server/config.ini

chown root:nginx -R /var/www/linx
chmod 770 -R /var/www/linx
chown root:nginx -R /opt/linx-server/meta
chmod 770 -R /opt/linx-server/meta

echo "Starting linx"

systemctl daemon-reload
systemctl restart linx-server.service

echo "Restart nginx"

nginx -t
systemctl restart nginx.service
