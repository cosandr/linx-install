#!/bin/bash

set -e

# Find latest version from GitHub
latest_tag=$(curl --silent "https://api.github.com/repos/andreimarcu/linx-server/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")')
latest_ver=${latest_tag:1}
install_bin="/opt/linx-server/linx-server"
version_file="/opt/linx-server/version.txt"
download_url="https://github.com/andreimarcu/linx-server/releases/latest/download/linx-server-v${latest_ver}_linux-amd64"

installed_ver=""
if [ -f $version_file ]; then
    installed_ver=$(<"$version_file")
fi

if [ "$latest_ver" == "$installed_ver" ]; then
    echo "No update needed."
else
    echo "Updating from $installed_ver to $latest_ver"
    wget -qO "/tmp/linx-server" "$download_url"
    if [[ $? -ne 0 ]]; then
        echo "Could not download Linx Server $latest_tag"
        echo "Download URL $download_url"
        exit 1
    else
        systemctl stop linx-server.service
        mv /tmp/linx-server "$install_bin"
        chmod 755 "$install_bin"
        restorecon -v "$install_bin"
        systemctl start linx-server.service
        echo "$latest_ver" > "$version_file"
        chmod 644 "$version_file"
        echo "Linx Server upgrade complete."
    fi
fi
